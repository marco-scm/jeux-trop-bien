<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\User;
use App\Form\ResetPasswordType;
use App\Form\ResettingType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

use Doctrine\ORM\EntityManagerInterface;
use App\Service\Mailer;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\EmailType;


class ResetPasswordController extends AbstractController
{
    /**
     * @Route("/reset-password", name="askResetPassword")
     */
    public function askReset(Request $request, Mailer $mailer, TokenGeneratorInterface $tokenGenerator)
    {
       // $user = new User();
        $form = $this->createForm(ResetPasswordType::class);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $user = $em->getRepository(User::class)->loadUserByUsername($form->getData()['email']);

            /* je récupère le mot de passe soumis*/
            $email = $user->getEmail();
            
            /*Je vérifie que cet email existe dans la bdd*/
            $emailCheck = $em->getRepository(User::class)->findby([
	        'email' => $email,	       
	         ]);
       
            if(!$emailCheck){
            	 $this->addFlash('error', ' Cet email n\'est associé à aucun compte !');
            	 return $this->redirectToRoute('askResetPassword');
            }
            else{

            	//$user = $this->getUser();
            	// création du token
            	 $user->setToken($tokenGenerator->generateToken());
                // dd($user);
	            // enregistrement de la date de création du token

	            $user->setPasswordRequestedAt(new \Datetime());

               // dd($user);
	            $em->flush();


	            // on utilise le service Mailer créé précédemment
	            $bodyMail = $mailer->createBodyMail('reset/mail.html.twig', [
	                'user' => $user
	            ]);
	            $mailer->sendMessage('demo@bes-webdeveloper-seraing.be', $user->getEmail(), 'renouvellement du mot de passe', $bodyMail);

                //dd($mailer);
                
	            
	            $this->addFlash('success', "Un mail vous a été envoyé afin que vous puissiez renouveller votre mot de passe. Le lien sera valide 24h.");

                ($bodyMail);

	            return $this->redirectToRoute("askResetPassword");
	            
	            //dd($emailCheck);

            }
           // dd($emailCheck);


        }
        

        return $this->render('reset/askResetPassword.html.twig', [
            'form' => $form->createView()
        ]);
    }



     // si supérieur à 10min, retourne false
    // sinon retourne false
    private function isRequestInTime(\Datetime $passwordRequestedAt = null)
    {
        if ($passwordRequestedAt === null)
        {
            return false;        
        }
        
        $now = new \DateTime();
        $interval = $now->getTimestamp() - $passwordRequestedAt->getTimestamp();

        $daySeconds = 60 * 10;
        $response = $interval > $daySeconds ? false : $reponse = true;
        return $response;
    }

    /**
     * @Route("/{id}/{token}", name="resetting")
     */
    public function resetting(User $user, $token, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // interdit l'accès à la page si:
        // le token associé au membre est null
        // le token enregistré en base et le token présent dans l'url ne sont pas égaux
        // le token date de plus de 10 minutes
        if ($user->getToken() === null || $token !== $user->getToken() || !$this->isRequestInTime($user->getPasswordRequestedAt()))
        {
            return $this->redirectToRoute("askResetPassword");
            //throw new AccessDeniedHttpException();
        } 

        $form = $this->createForm(ResettingType::class, $user);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            // réinitialisation du token à null pour qu'il ne soit plus réutilisable
            $user->setToken(null);
            $user->setPasswordRequestedAt(null);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $request->getSession()->getFlashBag()->add('success', "Votre mot de passe a été renouvelé.");
            //$this->addFlash('success', "Votre mot de passe a été renouvelé.");

            return $this->redirectToRoute('login');

        }

        return $this->render('reset/resetting.html.twig', [
            'form' => $form->createView()
        ]);
        
    }

}


?>