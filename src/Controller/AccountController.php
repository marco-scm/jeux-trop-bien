<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;
use App\Form\UserType;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Form\FormError;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request; // permet de centraliser l'accès à toutes les super variables de PHP en une seule classe utilitaire
use Symfony\Component\HttpFoundation\Response;



class AccountController extends AbstractController
{

 /**
     * @Route("/profile/compte", name="compte")
     */
    public function compte(Request $request)
    {

        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            $email=$user->getEmail();

            $em=$this->getDoctrine()->getManager();
            $check = $em->getRepository(user::class)->findby([
            'email' => $email,

            
             ]);
              //dd($check);
            if ($check == null){
           
                 $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'Votre email à été enregistré avec succès.');
            }
            else{
                  $this->addFlash('error', 'Cet email existe déja !');
            }

            return $this->redirectToRoute('compte');
        }



        return $this->render('main/compte.html.twig', [
             //'password' => $password,
             'user' => $user,
            'form' => $form->createView(),
           
        ]);
    }


     /**
     * @Route("/profile/compte/delete-email", name="deleteEmail")
     */
    public function deleteEmail()
    {

        $user = $this->getUser();
      

            $email=$user->setEmail('');

        $this->getDoctrine()->getManager()->flush();
         return $this->redirectToRoute('compte');

    }

}