<?php

namespace App\Controller;

use App\Entity\Jeux;
use App\Entity\ImagesAlt;
use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use App\Form\JeuxType;
use App\Service\FileUploader;
use App\Service\FileUploaderAlt;
use App\Repository\JeuxRepository;
use App\Entity\Comments;
use App\Repository\CommentsRepository;
use App\Repository\ImagesAltRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class JeuxController extends AbstractController
{
    /**
     * @Route("/", name="jeux_index", methods={"GET"})
     */
    public function index(JeuxRepository $jeuxRepository, CategoryRepository $categoryRepository): Response
    {

        return $this->render('jeux/index.html.twig', [
            'jeuxes' => $jeuxRepository->findAll(),
            'categories' => $categoryRepository->findAll(),
            
        ]);
    }

    /**
     * @Route("/new", name="jeux_new", methods={"GET","POST"})
     */
    public function new(Request $request, FileUploader $fileUploader, FileUploaderAlt $fileUploaderAlt, CategoryRepository $categoryRepository): Response
    {
        $jeux = new Jeux();
        $form = $this->createForm(JeuxType::class, $jeux);
        $form->handleRequest($request);


        $category = new Category();
        $formCat = $this->createForm(CategoryType::class, $category);
        $formCat->handleRequest($request);



         if ($formCat->isSubmitted() && $formCat->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $category->setNomCat($category);

            $entityManager->persist($category);
            $entityManager->flush();

         }


        if ($form->isSubmitted() && $form->isValid()) {


            /******             Uploads                         ******/
          
            $image = $form['image']->getData();
           
         



            if ($image) {
                $imageFileName = $fileUploader->upload($image);
                 $jeux->setimage('/symfony/uploads/images/' .$imageFileName);
            }

            $imageAlt = new ImagesAlt();
            $image2 = $form->get('image2')->getData();        
              
            if ($image2) {
               
                $image2FileName = $fileUploaderAlt->upload($image2);
                 $imageAlt->setImage2('/symfony/uploads/alt_images/' .$image2FileName);
                 $jeux->setImageAlt($imageAlt);
            }

            $image3 = $form->get('image3')->getData(); 

            if ($image3) {
               
                $image3FileName = $fileUploaderAlt->upload($image3);
                 $imageAlt->setImage3('/symfony/uploads/alt_images/' .$image3FileName);
                 $jeux->setImageAlt($imageAlt);
            }
        

            /****                Ends upload                              ****/




            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($jeux);
            $entityManager->flush();

            return $this->redirectToRoute('jeux_index');
        }

        return $this->render('jeux/new.html.twig', [
            'jeux' => $jeux,
            'category' => $categoryRepository->findAll(),
            'formCat' => $formCat->createView(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="jeux_show", methods={"GET"})
     */
    public function show(Jeux $jeux): Response
    {

        $em=$this->getDoctrine()->getManager();
        $jeuxcat = $em->getRepository(Jeux::class)->find('1');

        return $this->render('jeux/show.html.twig', [
            'jeux' => $jeux,
            'jeuxcat' => $jeuxcat,
            
        ]);
    }

    /**
     * @Route("/{id}/edit", name="jeux_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Jeux $jeux, FileUploader $fileUploader, FileUploaderAlt $fileUploaderAlt, CategoryRepository $categoryRepository): Response
    {
        $form = $this->createForm(JeuxType::class, $jeux);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {

            /******             Uploads                         ******/

            $image = $form['image']->getData();
           

        if ($image) {
            $imageFileName = $fileUploader->upload($image);
             $jeux->setimage('/symfony/uploads/images/' .$imageFileName);
        }

        $imageAlt=$jeux->getImageAlt();
        $image2 = $form->get('image2')->getData();        
          
        if ($image2) {
           
            $image2FileName = $fileUploaderAlt->upload($image2);
             $imageAlt->setImage2('/symfony/uploads/alt_images/' .$image2FileName);
             $jeux->setImageAlt($imageAlt);
        }

        $image3 = $form->get('image3')->getData(); 

        if ($image3) {
           
            $image3FileName = $fileUploaderAlt->upload($image3);
             $imageAlt->setImage3('/symfony/uploads/alt_images/' .$image3FileName);
             $jeux->setImageAlt($imageAlt);
        }
        
        
        /******             Ends Uploads Edit                         ******/

            
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('jeux_index');
        }

        return $this->render('jeux/edit.html.twig', [
            'jeux' => $jeux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="jeux_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Jeux $jeux): Response
    {
        if ($this->isCsrfTokenValid('delete'.$jeux->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($jeux);
            $entityManager->flush();
        }

        return $this->redirectToRoute('jeux_index');
    }




    
}
