<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\JeuxRepository;
use App\Entity\Jeux;

use App\Entity\Comments;
use App\Form\CommentsType;

use App\Entity\Vote;

use App\Entity\Category;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request; // permet de centraliser l'accès à toutes les super variables de PHP en une seule classe utilitaire
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class CommentsVotesController extends AbstractController
{
   

    /**
     * @Route("/category/{id}", name="category_id", methods={"GET","POST"})
     */
    public function showCategoryId(Request $request, Jeux $jeux, Security $security, EntityManagerInterface $manager): Response
    {


        $comments = new Comments();
        $form = $this->createForm(CommentsType::class, $comments);
        $form->handleRequest($request);


        
		$commentaires = $this->getDoctrine()->getRepository(Comments::class)->findBy([
        'jeux' => $jeux,
         ]);

        $votes = $this->getDoctrine()->getRepository(Vote::class)->findBy([
        'jeux' => $jeux,
         ]);

 
        $em=$this->getDoctrine()->getManager();
        $jeuxcat = $em->getRepository(Jeux::class)->find('1');
    


       $user = $this->getUser();
       // var_dump($voteValue);


 		if ($form->isSubmitted() && $form->isValid()) {

        
            /* Je vérifie que le commetnaire ne soit pas vide */
            if ($comments != null){
        
                /*Vérification que il n'essaie pas de commenté après trop longtemp sinon il commentera déconnecté (ça se déconnecte tous seul)*/
                $ValidTimeOut = false;
                $roles=$user->getRoles();
                foreach($roles as $element)
                {
                    if (($element == "ROLE_ADMIN") || ($element == "ROLE_USER")){
                        $ValidTimeOut = true;
                    }
                }
                if ($ValidTimeOut == true){
     			

         			$comments->setJeux($jeux);
         			$comments->setUser($user);
         			$comments->setCreateAt(new \DateTime());
                            
                   	$manager->persist($comments);
                    $manager->flush();
                }
            }
          
            return $this->redirectToRoute('category_id', ['id' => $jeux->getId()]);

 		}


        /*Je vérifie qu'il n'a pas déja voté, qu'il revient sur la page précédents pout voter */
        $em=$this->getDoctrine()->getManager();
        $check = $em->getRepository(Vote::class)->findby([
        'user' => $user,
        'jeux' => $jeux,
         ]);
         

        $vote = new Vote();
        $voteValue=$request->get('rating');
        $voteInt = (int)$voteValue;

            /* Je vérifie qu'il attribue bien une note */
        if ($voteValue != null){
    
            /*Vérification que il n'essaie pas de voté après trop longtemp sinon il votera déconnecté (ça se déconnecte tous seul)*/
            $ValidTimeOut = false;
                $roles=$user->getRoles();
                foreach($roles as $element)
                {
                    if (($element == "ROLE_ADMIN") || ($element == "ROLE_USER")){
                        $ValidTimeOut = true;
                    }
                }
            if ($ValidTimeOut == true){

                /* vérification qu'il n'as déja pas voté ce jeu*/
                if ($check == null){

                $vote->setJeux($jeux);
                $vote->setUser($user);
     
                $vote-> setVoteValue($voteInt);
                
                $manager->persist($vote);
                $manager->flush();
                }
                else{
                    $this->addFlash('warning', 'Vous avez déja noté ce jeu.');
                }
             }
            //var_dump($vote);
            return $this->redirectToRoute('category_id', ['id' => $jeux->getId()]);

           // }


        }
      



        return $this->render('main/produit.html.twig', [
            
            //'categories' => $category,
            'jeuxcat' => $jeuxcat,
            'jeux' => $jeux,
            'commentaires' => $commentaires,
             'votes' => $votes,
             'user' => $user,
            'form' => $form->createView()    
        
        ]);
    }



    /**
     * @Route("/profile/compte/mes-avis", name="myComments")
     */
    public function myComments()
    {

         $user = $this->getUser();

         $comments = $this->getDoctrine()->getRepository(Comments::class)->findBy([
        'user' => $user,
         ]);

        return $this->render('main/myComments.html.twig', [
            'comments' => $comments,
        ]);
    }


    /**
     * @Route("/profile/compte/mes-votes", name="myVotes")
     */
    public function myVotes()
    {

         $user = $this->getUser();

         $votes = $this->getDoctrine()->getRepository(Vote::class)->findBy([
        'user' => $user,
         ]);

        return $this->render('main/myVotes.html.twig', [
            'votes' => $votes,
            
        ]);
    }

    /**
     * @Route("/profile/compte/votes-delete/{id}", name="deleteVotes", methods={"DELETE"})
     */
    public function deleteVotes(Request $request, Vote $vote): Response
    {
        if ($this->isCsrfTokenValid('delete'.$vote->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($vote);
            $entityManager->flush();
        }

        return $this->redirectToRoute('myVotes');

        
    }

    /**
     * @Route("/profile/compte/comments-delete/{id}", name="deleteComments", methods={"DELETE"})
     */
    public function deleteComments(Request $request, Comments $comments): Response
    {
        if ($this->isCsrfTokenValid('delete'.$comments->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comments);
            $entityManager->flush();
        }

        return $this->redirectToRoute('myComments');

        
    }

    /**
     * @Route("/admin/all-reports", name="allReports")
     */
    public function allReports()
    {

         $user = $this->getUser();

         //$comments = $this->getDoctrine()->getRepository(Comments::class)->findAll();
          $comments = $this->getDoctrine()->getRepository(Comments::class)->findMostReports();
       // dd($reports);

        return $this->render('reports/report.html.twig', [
            'comments' => $comments,
        ]);
    }

    /**
     * @Route("/admin/all-reports/{id}", name="deleteCommentsAdmin", methods={"DELETE"})
     */
    public function deleteCommentsAdmin(Request $request, Comments $comments): Response
    {
        if ($this->isCsrfTokenValid('delete'.$comments->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comments);
            $entityManager->flush();
        }

        return $this->redirectToRoute('allReports');

        
    }



}
