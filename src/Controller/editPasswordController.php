<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Entity\User;
use App\Form\EditPasswordType;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
//use Symfony\Component\Form\FormError;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request; // permet de centraliser l'accès à toutes les super variables de PHP en une seule classe utilitaire
use Symfony\Component\HttpFoundation\Response;



class editPasswordController extends AbstractController
{


    /**
     * @Route("/profile/compte/edit-password", name="editPassword")
     */
    public function editPassword(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {

         $em1 = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $formPass = $this->createForm(EditPasswordType::class, $user);

        $formPass->handleRequest($request);

        //dd($formPass);

        if ($formPass->isSubmitted() && $formPass->isValid()) {

            // dump($request->request);die();
            $currentPassword = $request->request->get('edit_password')['currentPassword'];

            // Si l'ancien mot de passe est bon
            if ($passwordEncoder->isPasswordValid($user, $currentPassword)) {

                $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $formPass->get('newPassword')->getData()
                )
            );

                
                
                $em1->persist($user);
                $em1->flush();

                $this->addFlash('success', 'Votre mot de passe à bien été changé !');

                //return $this->redirectToRoute('compte');
            } else {
                //$form->addError(new FormError('Ancien mot de passe incorrect'));
                $this->addFlash('error', ' Mot de passe actuel incorrect !');
            }


        }
        else if ( $formPass->isSubmitted() ){
            $this->addFlash('error', ' Le mot de passe doit contenir minimum 6 caratères !');
        }
        

        

       // $user = $this->getUser();

        return $this->render('main/editPassword.html.twig', [
             //'password' => $password,
             'user' => $user,
           
            'formPass' => $formPass->createView(),
        ]);
    }


}




