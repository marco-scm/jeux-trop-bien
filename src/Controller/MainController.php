<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\JeuxRepository;
use App\Entity\Jeux;
use App\Entity\PropertySearch;
use App\Form\PropertySearchType;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;

use App\Entity\User;
use App\Form\UserType;
use App\Form\EditPasswordType;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Form\FormError;



use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request; // permet de centraliser l'accès à toutes les super variables de PHP en une seule classe utilitaire
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface; // bundle KNP Paginator


class MainController extends AbstractController
{


    /**
     * @Route("/", name="accueil")
     */
    public function index()
    {

        $donnees = $this->getDoctrine()->getRepository(Jeux::class)->findBy([],['id' => 'desc']);

        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
            'jeux' => $donnees,

        ]);
    }
    

    /**
     * @Route("/category", name="category")
     *
     * affiche le propertyform + les jeux + la pagination sur page category
     */
    public function indexPaginator(Request $request, PaginatorInterface $paginator, JeuxRepository $jeuxRepository) // Nous ajoutons les paramètres requis
    {
       // $session->get('currentQuery');

        $search = new PropertySearch();
        $form = $this->createForm(PropertySearchType::class, $search);
        $form->handleRequest($request);

        $jeuxParPage = 12;
      
            $firstTri = $request->get('firstTri');
            $numberTri = $request->get('numberTri');
            $categorie = $request->get('categorie');
            $nbJoueur = $request->get('nbJoueur');
            $duree = $request->get('duree');
            $age = $request->get('age');
            $motscle=$request->get('search');
            $prix=$request->get('prix');
            $categories=$request->get('jeuxes');
            
            $criteria = $form->getData();

        

            /*Je revérifie le nombres de jeux par page au cas où le get est modifier par un moyen quelconque */
             if (($numberTri == 12) || ($numberTri == 15) || ($numberTri == 18)){

                $jeuxParPage = $numberTri; 

            }
       

        if ( isset($categories) || isset($nbJoueur)  || isset($duree) || isset($age) || isset($firstTri) || isset($motscle) || isset($prix)){

            if (($categories != null) || ($nbJoueur != null) || ($duree != null) || ($age != null)  || ($firstTri != null)  || ($motscle != '')  || ($prix != null)){

                 $donnees = $this->getDoctrine()->getRepository(Jeux::class)->findJeuxBytri($nbJoueur, $duree, $age, $firstTri, $motscle, $categories,$prix ); 
            }
            else {

               
                    $donnees = $this->getDoctrine()->getRepository(Jeux::class)->findBy([],['id' => 'desc']);
                    
            }
            
        }

        else {

                 // Méthode findBy qui permet de récupérer les données avec des critères de filtre et de tri
                //Si aucun critères de recherches n'est actif je fais appel à la fonction findBy simplement
             
                    $donnees = $this->getDoctrine()->getRepository(Jeux::class)->findBy([],['id' => 'desc']);
              
              
            }


        $nbjeux = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)

            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            $jeuxParPage // Nombre de résultats par page
        );


        return $this->render('main/category.html.twig', [
            'jeux' => $nbjeux,
            'search' => $motscle,
            'form' => $form->createView()
        ]);
    }



    /**
     * @Route("/connexion", name="login")
     */
    public function connexion()
    {
        return $this->render('main/connexion.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }


   

    /**
     * @Route("/produit", name="produit")
     */
    public function produit()
    {
        return $this->render('main/produit.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }


    /**
     * @Route("/recherche", name="recherche")
     */
    public function recherche()
    {
        return $this->render('main/recherche.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }


    /**
     * page logout
     *
     *@Route("/logout", name="logout")
     */
    public function logout(){

       $this->addFlash('success', 'Vous êtes déconnecté, à bientôt!');  
       return $this->render('main/index.html.twig');  

        
    }

}




