<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\JeuxRepository;
use App\Entity\Jeux;
use App\Entity\PropertySearch;
use App\Form\PropertySearchType;
use Symfony\Component\HttpFoundation\Request; // permet de centraliser l'accès à toutes les super variables de PHP en une seule classe utilitaire

use Knp\Component\Pager\PaginatorInterface; // Nous appelons le bundle KNP Paginator



class PropertySearchController extends AbstractController{


	/**
     * @Route("/tri", name="tri")
     */
     public function propertyFilter(Request $request, PaginatorInterface $paginator, JeuxRepository $jeuxRepository) // Nous ajoutons les paramètres requis
    {

    	$search = new PropertySearch();
        $form = $this->createForm(PropertySearchType::class, $search);
        $form->handleRequest($request);


      $name=$request->get('catri');
    	 $criteria = $form->getData();
    	 $var = $request->request->all();




        // Méthode findBy qui permet de récupérer les données avec des critères de filtre et de tri
        $donnees = $this->getDoctrine()->getRepository(Jeux::class)->findBy( array('id' => $motscle));

        $nbjeux = $paginator->paginate(
            $donnees, // Requête contenant les données à paginer (ici nos articles)

            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            12 // Nombre de résultats par page
        );




        return $this->render('main/tri.html.twig', [
            'jeux' => $nbjeux,
            'form' => $form->createView()
            
        ]);
    }

}