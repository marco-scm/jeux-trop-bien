<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/inscription", name="inscription")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            //$user->setIsActive(true);
             $rolesArr = array('ROLE_USER');
             //$rolesArr = array('ROLE_ADMIN');
             $user->setRoles($rolesArr);
            //$user->setRoles("ROLE_ADMIN");

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();
            

            // do anything else you need here, like send an email
             $this->addFlash('success', 'Votre compte à bien été enregistré.');
            //return $this->redirectToRoute('');
        }
/*
        return $this->render('main/inscription.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
        */
        return $this->render('main/inscription.html.twig',
         ['form' => $form->createView(), 'mainNavRegistration' => true, 'title' => 'Inscription']);
    }

}
