<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Jeux;
use App\Repository\UserRepository;
use App\Entity\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/all-membres")
 */
class ManageMembreController extends AbstractController
{
    
    /**
     * @Route("/User", name="Membres", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {



        return $this->render('main/membre.html.twig', [
            'users' => $userRepository->findAll(),
            //'categories' => $categoryRepository->findAll(),
            
        ]);
    }


    /**
     * @Route("/restrict/{id}", name="restricted")
     */
    /*
    public function restricted(Request $request): Response
    {
        $user=$this->getUser();
        $ValidTimeOut = false;
        $roles=$user->getRoles();
        foreach($roles as $element)
        {
            if ($element == "ROLE_ADMIN"){
                $ValidTimeOut = true;
            }
        }
        if ($ValidTimeOut == true){

            $em1 = $this->getDoctrine()->getManager();
            $user->setRestricted(new \Datetime());

               // $em1->persist($user);
                $em1->flush();
         
           
        }
       

        return $this->redirectToRoute('Membres');

        
    }
    */

}