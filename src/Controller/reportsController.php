<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
//use App\Repository\JeuxRepository;
use App\Entity\Jeux;
use App\Entity\User;


use App\Entity\Comments;
use App\Form\CommentsType;
use App\Repository\CommentsRepository;

use App\Entity\Category;


use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request; // permet de centraliser l'accès à toutes les super variables de PHP en une seule classe utilitaire
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class reportsController extends AbstractController
{
   

    /**
     * @Route("/profile/signaler/{id}", name="signaler")
     */
    public function signaler(Request $request, Comments $comments): Response
    {
         $user=$this->getUser();
        $ValidTimeOut = false;
        $roles=$user->getRoles();
        foreach($roles as $element)
        {
            if (($element == "ROLE_ADMIN") || ($element == "ROLE_USER")){
                $ValidTimeOut = true;
            }
        }
        if ($ValidTimeOut == true){
                
          $jeux = $comments->getJeux();
          $jeux_id= $jeux->getId();
         
            $entityManager = $this->getDoctrine()->getManager();
            $reports = $comments->getReports();
            $reports=$reports+1;
            $reports=$comments->setReports($reports);
            
            $entityManager->flush();
            $this->addFlash('success', 'Signalement enregistré');
        }
       

        return $this->redirectToRoute('category_id', ['id' => $jeux_id]);

        
    }





}
