<?php

namespace App\Form;
use App\Entity\Category;
use App\Entity\PropertySearch;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PropertySearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)

    {
        $builder

            ->add('firstTri', ChoiceType::class, [
                'label' => false,
                'choices' => [

                'Date d\'ajout' => '1',
                'Le moins cher' => '2',
                'Le plus cher' => '3',
                'Ordre alphabétique' => '4',

                ]
            ])
            

            ->add('numberTri', ChoiceType::class, [
                'label' => false,
                'choices' => [

                'Trié par 12' => '12',
                'Trié par 15' => '15',
                'Trié par 18' => '18',
                ]
             ])

/*
            ->add('categorie',ChoiceType::class,array(
                'choices'  => array(
                    'Jeux de plateau' => 'plateau',
                    'Jeux de tactique' => 'tactique',
                    'Jeux de mémoire' => 'memoire',
                    'Jeux de logique' => 'logique',
                    'Jeux de dés' => 'des',
                    'Jeux éducatifs' => 'educ',
                    'Toute catégorie ' => 'all',


                ),

                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'placeholder' => false
                
            ))
            */

            ->add('jeuxes', EntityType::class,[
                'required' => false,
                'label' => false,
                'class' =>  Category::class,
                'choice_label' => 'nom_cat',
                'expanded' => true,
                'multiple' => true,
                

            ])

            ->add('nbJoueur',ChoiceType::class,array(
                'choices'  => array(
                    '1 joueur' => 1,
                    'Jusque 2 joueurs' => 2,
                    'Jusque 4 joueurs' => 4,
                    'Jusque 10 joueurs' => 10,
                    '+ de 10 joueurs' => 11,
                    'Tous' => 12,
                    
                ),
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'placeholder' => false
            ))

            ->add('duree',ChoiceType::class,array(
                'choices'  => array(
                    'Moins de 30 min' => 1,
                    '30 min à 1h' => 2,
                    '1 à 2h' => 3,
                    '2 à 3h' => 4,
                    '3 à 4h' => 5,
                    '4h et +' => 6,
                    'Toute durée' => 7,
                    
                ),
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'placeholder' => false
            ))


            ->add('age',ChoiceType::class,array(
                'choices'  => array(
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                    '4' => 4,
                    '5' => 5,
                    '6' => 6,
                    '7' => 7,
                    '8' => 8,
                    '9' => 9,
                    '10' => 10,
                    '11' => 11,
                    '12' => 12,
                    '13' => 13,
                    '14' => 14,
                    '15' => 15,
                    '16' => 16,
                    '17' => 17,
                    '18' => 18, 
                    'Tout âge' => 19,
    
                ),
                'expanded' => true,
                'multiple' => false,
                'required' => false,
                'placeholder' => false
            ))

            ->add('prix', RangeType::class, [
                'attr' => [
                    'min' => 10,
                    'max' => 200,
                     'step'  => 5,
                    'value' => 200,
                    
                ]
            ])
            


            ->add('tri', SubmitType::class, [
                'label' => 'Appliquer',
                ]);
            
            

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PropertySearch::class,
            'method' => 'get',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
         
    }

    

}





