<?php

namespace App\Form;

use App\Entity\Jeux;
use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JeuxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('nom')
            ->add('age_min')
            ->add('nb_joueur_min')
            ->add('nb_joueur_max')
            ->add('duree')
            ->add('prix')
            //->add('image')
            ->add('description', TextareaType::class)

            ->add('categories', EntityType::class,[
                'class' => Category::class,
                'choice_label' => 'nom_cat',
                'multiple' => true


                ])
                

            // ->add('plateau')
            // ->add('tactique')
            // ->add('memoire')
            // ->add('logique')
            // ->add('des')
            // ->add('educ')
            //->add('image2',  ImagesAltType::class, ['mapped' => false, 'label' => false])

          //{{ form_enctype(form) }}
      

            ->add('image', FileType::class, [
                'label' => 'Image',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // everytime you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Veulliez choisir une image de type png ou jpeg.',
                    ])
                ],
            ])

            ->add('image2', FileType::class, [
                'label' => 'Image2',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // everytime you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Veulliez choisir une image de type png ou jpeg.',
                    ])
                ],
            ])

            ->add('image3', FileType::class, [
                'label' => 'Image3',

                // unmapped means that this field is not associated to any entity property
                'mapped' => false,

                // make it optional so you don't have to re-upload the PDF file
                // everytime you edit the Product details
                'required' => false,

                // unmapped fields can't define their validation using annotations
                // in the associated entity, so you can use the PHP constraint classes
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/png',
                            'image/jpeg',
                        ],
                        'mimeTypesMessage' => 'Veulliez choisir une image de type png ou jpeg.',
                    ])
                ],
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Jeux::class,
        ]);
    }
}
