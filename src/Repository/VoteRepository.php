<?php

namespace App\Repository;

use App\Entity\Vote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Vote|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vote|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vote[]    findAll()
 * @method Vote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vote::class);
    }


    public function findJeuxPopular()
    {
       $qb = $this->createQueryBuilder('v');

        $qb//->addselect('avg(v.vote_value) as decimal')
            ->addSelect('v.vote_value')
            ->where('v.vote_value >= 0')
            ->groupBy('v.jeux_id')
            ->orderBy('v.vote_value', 'DESC')
           ->getQuery()
            
        ;
    

    $query = $qb->getQuery();
   

        return $query->getResult();
       
        return $qb->getQuery();


    }
}
