<?php

namespace App\Repository;

use App\Entity\Comments;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Comments|null find($id, $lockMode = null, $lockVersion = null)
 * @method Comments|null findOneBy(array $criteria, array $orderBy = null)
 * @method Comments[]    findAll()
 * @method Comments[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comments::class);
    }

    // /**
    //  * @return Comments[] Returns an array of Comments objects
    //  */
    
    public function findMostReports()
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.reports > 0')
            ->orderBy('c.reports', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
/*
    $query = $this->createQueryBuilder('v');

        $query->select('sum(v.vote_value)')
            ->addSelect('v.jeux_id')
            ->where('v.vote_value >= 0')
            ->groupBy('v.vote_value')
            ->orderBy('v.vote_value', 'DESC')
           ->getQuery()
            //->getResult()
        ;
    */

    /*
    public function findOneBySomeField($value): ?Comments
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
