<?php

namespace App\Repository;

use App\Entity\ImagesAlt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;


/**
 * @method ImagesAlt|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImagesAlt|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImagesAlt[]    findAll()
 * @method ImagesAlt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImagesAltRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImagesAlt::class);
    }

    // /**
    //  * @return ImagesAlt[] Returns an array of ImagesAlt objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ImagesAlt
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getImageAlt()
    {
    $repository = $this->getDoctrine()
                   ->getManager()
                   ->getRepository('ImageAltRepository');
    }

 
}
