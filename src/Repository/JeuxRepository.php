<?php

namespace App\Repository;

use App\Entity\Jeux;
use App\Entity\Vote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Jeux|null find($id, $lockMode = null, $lockVersion = null)
 * @method Jeux|null findOneBy(array $criteria, array $orderBy = null)
 * @method Jeux[]    findAll()
 * @method Jeux[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JeuxRepository extends ServiceEntityRepository
{



    //private $session;

    public function __construct(ManagerRegistry $registry) //SessionInterface $session
    {
        parent::__construct($registry, Jeux::class);
       // $this->session = $session;
    }
    

    

    public function findJeuxBynom($motscle){

         $query = $this->createQueryBuilder('j')
            ->andWhere('j.nom like :nom')
            ->setParameter('nom', '%'.$motscle.'%')
            ->orderBy('j.nom', 'ASC')
            //->setMaxResults(10)
            ->getQuery()
            //->getResult()
        ;
        return $query->getResult();

    }





    public function findJeuxBytri($nbJoueur, $duree, $age, $firstTri, $motscle, $categories,$prix){
        // first, create the query builder
    $qb = $this->createQueryBuilder('j')
        ->select('j')
        
        
    ;

        //if ($categories->getJeuxes()->count() > 0){
    if (isset($categories)){
        $k=0;
         foreach($categories as $k => $category){
            $k++;
             $qb->orWhere(":category$k MEMBER OF j.categories")->setParameter("category$k", $category);
         }
    }

    /* Recherche en fonction de l'age */
    if (isset($age)){
        if ($age <= 18){
            $qb->andWhere('j.age_min <= :age')->setParameter('age', ''.$age.'');
        }
        else if ($age == 20){
            $qb->andWhere('j.age_min >= :age')->setParameter('age', 14);
        }
    }
    /* Recherche en fonction du nombres de joueurs */
    if (isset($nbJoueur) && ($nbJoueur != 12) ){
        if ($nbJoueur == 1){
            $qb->andWhere('j.nb_joueur_min = :nbJoueur')
            ->setParameter('nbJoueur', ''.$nbJoueur.'');   
        }
        if (($nbJoueur <= 10) && ($nbJoueur > 1)){
            $qb->andWhere('j.nb_joueur_max <= :nbJoueur')->setParameter('nbJoueur', ''.$nbJoueur.'');
        }
        if ($nbJoueur == 11){
            $qb->andWhere('j.nb_joueur_min >= :nbJoueur')
            ->setParameter('nbJoueur', ''.$nbJoueur.'');   
        }
    }

    /* Recherche en fonction de la durée */
    if (isset($duree) && ($duree < 7)){
        
            $qb->andWhere('j.duree = :duree')->setParameter('duree', ''.$duree.'');
        
    }
      /* Recherche en fonction de la catégorie 
    if (isset($categorie)){
        if($categorie != 'all'){
        
            $qb->andWhere('j.'.$categorie.' = :categorie')->setParameter('categorie', '1');
        }
        
    }
    */


    if($firstTri == 1){
        
           $qb//->addselect('avg(v.vote_value) as decimal')
           /*
            ->innerJoin(
                'App\Entity\Vote', 'v'
                
               
            )
             ->addselect('v.jeux_id, count(v.jeux_id) as counter')
           // ->andwhere('v.vote_value ')
           // ->addSelect(j , v)
           // ->andwhere('v.vote_value >= 0')
           // ->innerJoin(v.vote_value j)
            ->groupBy('v.vote_value')
            */
            ->orderBy('j.id ', 'desc')
           
            
        ;
    }
  


    if($firstTri == 2){
        
            $qb->orderBy('j.prix', 'ASC');
    }

    if($firstTri == 3){
        
            $qb->orderBy('j.prix', 'DESC');
    }
    if($firstTri == 4){
        
            $qb->orderBy('j.nom', 'ASC');
    }

    /* Recherche en fonction de la durée */
    if (isset($motscle) && ($motscle != '')){
        
            $qb->andWhere('j.nom like :nom')->setParameter('nom', '%'.$motscle.'%');
        
    }

     if (isset($prix)){
        if ($prix < 150 ){
            $qb->andWhere('j.prix <= :prix')->setParameter('prix', ''.$prix.'');
        }
    }
        

    

    // get the query
    $query = $qb->getQuery();
   // $this->session->set('lastquery', $query);
   // $session->set('lastQuery', $query);
    
    
    //$session->set('currentQuery', $sqldata);

        return $query->getResult();
         //return new Paginator($queryBuilder);
        return $qb->getQuery();

    }


    public function getJeux()
    {
    $repository = $this->getDoctrine()
                   ->getManager()
                   ->getRepository('JeuxRepository');
    }

     public function getAll($jeux){
        $jeux = $this->getDoctrine()->getRepository(Jeux::class)->findBy([],['nom' => 'asc']);
    }



}


