<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200117221933 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE images_alt_images_alt (images_alt_source INT NOT NULL, images_alt_target INT NOT NULL, INDEX IDX_2920BC63C59180E3 (images_alt_source), INDEX IDX_2920BC63DC74D06C (images_alt_target), PRIMARY KEY(images_alt_source, images_alt_target)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE images_alt_images_alt ADD CONSTRAINT FK_2920BC63C59180E3 FOREIGN KEY (images_alt_source) REFERENCES images_alt (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE images_alt_images_alt ADD CONSTRAINT FK_2920BC63DC74D06C FOREIGN KEY (images_alt_target) REFERENCES images_alt (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE images_alt DROP image2');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE images_alt_images_alt');
        $this->addSql('ALTER TABLE images_alt ADD image2 VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci');
    }
}
