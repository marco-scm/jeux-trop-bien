<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;


class PropertySearch
{

    /**
     * @var integer
     */
    private $firstTri;

    /**
     * @var integer
     */
    private $numberTri;

    /**
     * @var integer
     */
    private $categorie;


    /**
     * @var integer
     */
    private $nbJoueur;

        /**
     * @var integer
     */
    private $duree;


    /**
     * @var integer
     */
    private $age;

    /**
     * @var integer
     */
    private $prix;

    /**
     * @var ArrayCollection
     */
    private $jeuxes;

    public function __construct(){
        $this->jeuxes = new ArrayCollection();
    }


    public function getJeuxes(): ArrayCollection
    {
        return $this->jeuxes;
    }
    
    
    public function setJeuxes(ArrayCollection $jeuxes): void
    {
        $this->jeuxes = $jeuxes;
       
    }


    public function getFirstTri()
    {
        return $this->firstTri;
    }
    
    
    public function setFirstTri( $firstTri): self
    {
        $this->firstTri = $firstTri;
        return $this;
    }

    public function getNumberTri()
    {
        return $this->numberTri;
    }
    
    
    public function setNumberTri( $numberTri): self
    {
        $this->numberTri = $numberTri;
        return $this;
    }


    public function getCategorie()
    {
        return $this->categorie;
    }
    
    
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
        return $this;
    }


    public function getNbJoueur()
    {
        return $this->nbJoueur;
    }


    public function setNbJoueur( $nbJoueur): self
    {
        $this->nbJoueur = $nbJoueur;
        return $this;
    }

    public function getDuree()
    {
        return $this->duree;
    }


    public function setDuree($duree): self
    {
        $this->duree = $duree;
        return $this;
    }

    public function getAge()
    {
        return $this->age;
    }


    public function setAge($age): self
    {
        $this->age = $age;
        return $this;
    }

    public function getPrix()
    {
        return $this->prix;
    }
    
    
    public function setPrix($prix): self
    {
        $this->prix = $prix;
        return $this;
    }




} 