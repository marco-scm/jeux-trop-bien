window.requestAnimationFrame || (window.requestAnimationFrame = window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function (a) {
    window.setTimeout(a, 1E3 / 60)
});


function addEvent(a,b,c,d){a.addEventListener?a.addEventListener(b,c,d||!1):a.attachEvent('on'+b,c);}

function goTo(e){

	function doScroll(){
	
		var progress = (new Date - start) / duration;
		
		if (progress > 1) progress = 1;
		
		var delta = (progress < .5) ? Math.pow(2 * progress, 3) / 2 : (2 - Math.pow(2 * (1 - progress), 3)) / 2,
			result = (targetTop - from) * delta + from ;
		
		window.scrollTo(0, result)
		
		if (progress === 1) return;
		
		window.requestAnimationFrame(doScroll);
		
	}

	(e.preventDefault) ? e.preventDefault() : (e.returnValue = false);
	
	var target = (e.target || e.srcElement).hash.substring(1),
		targetTop = document.getElementById(target).offsetTop,
		from = window.pageYOffset || document.documentElement.scrollTop,
		start = new Date;
	
	doScroll();
	
}


var link = document.querySelectorAll(".card_area a"),
	duration = 1000,
	i = 0,
	l = link.length;

for(; i<l; i++) addEvent(link[i], "click", goTo, false);



/*

	// Get the container element
var btnContainer = document.getElementById("myDIV");

// Get all buttons with class="btn" inside the container
var btns = btnContainer.getElementsByClassName("btn");

// Loop through the buttons and add the active class to the current/clicked button
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
    var current = document.getElementsByClassName("active");
    current[0].className = current[0].className.replace(" active", "");
    this.className += " active";
  });
}
*/


/*
	// Get the container element
var btnTopVoter = document.getElementById("voter_active");
var btnBottomVoter = document.getElementById("review-tab");

btnTopVoter.addEventListener("click", function() {

	var current = document.getElementsByClassName("review_class");
	current.className = current.className.replace("review_class", "");
	this.className += " active";
});
*/


/*
  $(function() {
        $("#voter_active").click(function() {
            $("#review-tab").addClass('active');
        });
    });
*/



  $("#voter_active").click(function(){ // met le lien voter en actif et affiche la div review(via toggle)

    $("#review-tab").addClass('active');
    $("#review").addClass('show active');
 

    $("#contact").removeClass('active');
     $("#contact-tab").removeClass('show');
    $("#contact-tab").removeClass('active');

    $("#profile").removeClass('active');
    $("#profile-tab").removeClass('show');
    $("#profile-tab").removeClass('active');

  }); 


    $("#comment_active").click(function(){ // met le lien voter en actif et affiche la div review(via toggle)

    $("#contact-tab").addClass('active');
    $("#contact").addClass('show active');


    $("#review").removeClass('active');
    $("#review-tab").removeClass('show');
    $("#review-tab").removeClass('active');

    $("#profile").removeClass('active');
    $("#profile-tab").removeClass('show');
    $("#profile-tab").removeClass('active');

  }); 

    ////////////////

     $("[for='age_0']").addClass('age-choice');
     $("[for='age_1']").addClass('age-choice');
     $("[for='age_2']").addClass('age-choice');
     $("[for='age_3']").addClass('age-choice');
     $("[for='age_4']").addClass('age-choice');
     $("[for='age_5']").addClass('age-choice');
     $("[for='age_6']").addClass('age-choice');
     $("[for='age_7']").addClass('age-choice');
     $("[for='age_8']").addClass('age-choice');
     $("[for='age_9']").addClass('age-choice');
     $("[for='age_10']").addClass('age-choice');
     $("[for='age_11']").addClass('age-choice');
     $("[for='age_12']").addClass('age-choice');
     $("[for='age_13']").addClass('age-choice');
     $("[for='age_14']").addClass('age-choice');
     $("[for='age_15']").addClass('age-choice');
     $("[for='age_16']").addClass('age-choice');
     $("[for='age_17']").addClass('age-choice');
     $("[for='age_18']").addClass('age-choice');


     $("[for='age_0']").attr('id', '1');
     $("[for='age_1']").attr('id', '2');
     $("[for='age_2']").attr('id', '3');
     $("[for='age_3']").attr('id', '4');
     $("[for='age_4']").attr('id', '5');
     $("[for='age_5']").attr('id', '6');
     $("[for='age_6']").attr('id', '7');
     $("[for='age_7']").attr('id', '8');
     $("[for='age_8']").attr('id', '9');
     $("[for='age_9']").attr('id', '10');
     $("[for='age_10']").attr('id', '11');
     $("[for='age_11']").attr('id', '12');
     $("[for='age_12']").attr('id', '13');
     $("[for='age_13']").attr('id', '14');
     $("[for='age_14']").attr('id', '15');
     $("[for='age_15']").attr('id', '16');
     $("[for='age_16']").attr('id', '17');
     $("[for='age_17']").attr('id', '18');
     $("[for='age_18']").attr('id', '0');

     
//var label = document.getElementsByClassName("age-choice");

//var label = document.getElementsByClassName("age-choice");

  $(".age-choice").click(function(){ // 

      var label = document.getElementsByClassName("age-choice");


      for (var i = 0; i < label.length; i++) {

        label[i].addEventListener("click", function() {

          if($("label").hasClass("age-selected")==true){

            var current = document.getElementsByClassName("age-selected");

             current[0].className = current[0].className.replace("age-selected", "");

          ;}
            
        this.className += " age-selected ";


             var txt = document.getElementsByClassName("age-selected");
             var AttributeNode = txt[0].getAttribute("id");

             if (AttributeNode == 1){
               $('#text-age-choice').text(AttributeNode + " an");
             }
             else if (AttributeNode == 0){
               $('#text-age-choice').text("Tout âge");
             }
             else{
              $('#text-age-choice').text(AttributeNode + " ans");
             }
            
       
              
 



      });

      }


   

  });
/*

$('#firstTri').change(function(){
    $('form').submit();
});

$('#numberTri').change(function(){
    $('form').submit();
});

$('[type="checkbox"]').change(function(){
    $('form').submit();
});

$('[type="radio"]').change(function(){
    $('form').submit();
});

$('[type="range"]').change(function(){
    $('form').submit();
});

*/




/*
 
   jQuery(document).ready(function($){
    $('#firstTri').submit();
});
*/

/*
      $("#review-tab").click(function(){ // met le lien voter en actif et affiche la div review(via toggle)

    $("#review-tab").addClass('active');
    $("#review").addClass('show active');
  
    $("#contact").removeClass('active');
     $("#contact-tab").removeClass('show');
    $("#contact-tab").removeClass('active');

    $("#profile").removeClass('active');
    $("#profile-tab").removeClass('show');
    $("#profile-tab").removeClass('active');

  }); 



      $("#contact-tab").click(function(){ // met le lien voter en actif et affiche la div review(via toggle)

    $("#review-tab").addClass('active');
    $("#review").addClass('show active');


    $("#contact").removeClass('active');
     $("#contact-tab").removeClass('show');
    $("#contact-tab").removeClass('active');

    $("#profile").removeClass('active');
    $("#profile-tab").removeClass('show');
    $("#profile-tab").removeClass('active');

  }); 
*/



    //  if($("p").hasClass("intro")==true){$("span").text("Oui Il existe un élément p avec cette classe");}


